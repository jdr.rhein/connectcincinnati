from django.db import models


class Beneficiary(models.Model):
    name = models.CharField(max_length=100)
    dob = models.CharField(max_length=50)
    location = models.CharField(max_length=100)
    wallet = models.SmallIntegerField(default=0)
    phone_number = models.SmallIntegerField(default=0)


class Services(models.Model):
    SERVICE_CHOICES = [
        ('FO', 'Food'),
        ('CL', 'Clothing'),
        ('HO', 'Housing'),
        ('JT', 'Job Training'),
    ]
    name = models.CharField(max_length=2, choices=SERVICE_CHOICES)
    beneficiary = models.ForeignKey(Beneficiary, on_delete=models.CASCADE)


class Donation(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    service = models.ForeignKey(Services, on_delete=models.CASCADE)
    donor_name = models.CharField(max_length=100, blank=True)
    donation_date = models.DateTimeField(auto_now_add=True)

