from django.shortcuts import render
from .models import Beneficiary, Services, Donation
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import BeneficiaryEncoder, ServicesEncoder, DonationEncoder
import json


@require_http_methods(["GET", "POST"])
def list_beneficiary(request):
    if request.method == "GET":
        beneficiary = Beneficiary.objects.all()
        return JsonResponse(
            {'beneficiary': beneficiary},
            encoder=BeneficiaryEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        beneficiary = Beneficiary.objects.create(**content)
        return JsonResponse(
            {'beneficiary': beneficiary},
            encoder=BeneficiaryEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {'message': 'Customer not found'},
            status=400,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def details_beneficiary(request, pk):
    if request.method == "GET":
        beneficiary = Beneficiary.objects.get(pk=pk)
        return JsonResponse(
            beneficiary,
            encoder=BeneficiaryEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Beneficiary.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        Beneficiary.objects.filter(pk=pk).update(**content)
        beneficiary = Beneficiary.objects.get(pk=pk)
        return JsonResponse(
            beneficiary,
            encoder=BeneficiaryEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_services(request):
    if request.method == "GET":
        service = Services.objects.all()
        return JsonResponse(
            {"service": service},
            encoder=ServicesEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        beneficiary_id = content.pop("beneficiary", None)

        try:
            beneficiary = Beneficiary.objects.get(id=beneficiary_id)
        except Beneficiary.DoesNotExist:
            return JsonResponse(
                {"message": "provided ID does not exist"},
                status=400,
            )
        
        service = Services.objects.create(beneficiary=beneficiary, **content)

        return JsonResponse(
            {"service": service},
            encoder=ServicesEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"message": "Bad request"},
            status=400,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def details_services(request, pk):
    if request.method == "GET":
        try:
            service = Services.objects.get(pk=pk)
        except Services.DoesNotExist:
            return JsonResponse(
                {"message": "Service with the provided ID does not exist."},
                status=400,
            )

        return JsonResponse(
            {"service": service},
            encoder=ServicesEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Services.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    elif request.method == "PUT":
        try:
            service = Services.objects.get(pk=pk)
        except Services.DoesNotExist:
            return JsonResponse(
                {"message": "Service with the provided ID does not exist."},
                status=400,
            )

        content = json.loads(request.body)
        beneficiary_id = content.pop("beneficiary", None)

        try:
            beneficiary = Beneficiary.objects.get(id=beneficiary_id)
        except Beneficiary.DoesNotExist:
            return JsonResponse(
                {"message": "Beneficiary with the provided ID does not exist."},
                status=400,
            )

        # Update the fields of the existing service instance with the new data
        service.name = content.get("name", service.name)
        service.beneficiary = beneficiary

        # Save the updated service instance to the database
        service.save()

        return JsonResponse(
            {"service": service},
            encoder=ServicesEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"message": "Bad Request"},
            status=400,
        )


@require_http_methods(["GET", "POST"])
def list_donations(request):
    if request.method == "GET":
        donations = Donation.objects.all()
        return JsonResponse(
            {"donations": donations},
            encoder=DonationEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        service_id = content.pop("service", None)

        try:
            service = Services.objects.get(id=service_id)
        except Services.DoesNotExist:
            return JsonResponse(
                {"message": "provided ID does not exist"},
                status=400,
            )
        
        donation = Donation.objects.create(service=service, **content)

        return JsonResponse(
            {"donation": donation},
            encoder=DonationEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"message": "Bad request"},
            status=400,
        )
    

@require_http_methods(["GET", "PUT", "DELETE"])
def details_donations(request, pk):
    if request.method == "GET":
        try:
            donation = Donation.objects.get(pk=pk)
        except Donation.DoesNotExist:
            return JsonResponse(
                {"message": "no matching donation"},
                status=400
            )
        
        return JsonResponse(
            {"donation": donation},
            encoder=DonationEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Donation.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        try:
            donation = Donation.objects.get(pk=pk)
        except Donation.DoesNotExist:
            return JsonResponse(
                {"message": "provided id does not exist"},
                status=400,
            )

        content = json.loads(request.body)
        service_id = content.pop("service", None)

        try:
            service = Services.objects.get(id=service_id)
        except Services.DoesNotExist:
            return JsonResponse(
                {"message": "provided id does not exist"},
                status=400,
            )
        
        if "donor_name" in content:
            donation.donor_name = content.get("donor_name")
    
    # Update amount if provided
        if "amount" in content:
            donation.amount = content.get("amount")
        
        donation.save()

        return JsonResponse(
            {"donation": donation},
            encoder=DonationEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"message": "bad request"},
            status=400,
        )
    
