from django.apps import AppConfig


class ConnectApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'connect_api'
