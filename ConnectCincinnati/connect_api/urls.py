from django.urls import path
from .views import (list_beneficiary,
                    details_beneficiary,
                    list_services,
                    details_services,
                    list_donations,
                    details_donations
                    )


urlpatterns = [
    path('beneficiaries/', list_beneficiary, name='list_beneficiary'),
    path('beneficiaries/<int:pk>/', details_beneficiary, name='details_beneficiary'),
    path('services/', list_services, name='list_services'),
    path('services/<int:pk>', details_services, name='details_services'),
    path('donations/', list_donations, name='list_donations'),
    path('donations/<int:pk>', details_donations, name='details_donations'),
]
