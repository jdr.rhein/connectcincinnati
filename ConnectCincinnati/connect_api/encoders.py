from .models import Beneficiary, Services, Donation
from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from datetime import datetime
from decimal import Decimal


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
    

class BeneficiaryEncoder(ModelEncoder):
    model = Beneficiary
    properties = [
        "name",
        "dob",
        "location",
        "wallet",
        "phone_number",
        "id",
    ]


class ServicesEncoder(ModelEncoder):
    model = Services
    properties = [
        "name",
        'beneficiary',
        "id",
    ]

    def get_extra_data(self, o):
        return {"name": o.get_name_display(), "beneficiary": o.beneficiary.id}
    

class DonationEncoder(ModelEncoder):
    model = Donation
    properties = [
        "amount",
        "service",
        "donor_name",
        "donation_date",
        "id",
    ]

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)  # Convert Decimal to string representation
        return super(DonationEncoder, self).default(obj)
    
    def get_extra_data(self, o):
        return {
            "service": ServicesEncoder().default(o.service),
        }
